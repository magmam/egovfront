import React, { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { DotLoader } from 'react-spinners';
// import { Link } from "react-router-dom";
import classes from "./ManagerPage.module.scss";


const formatTimestamp = (timestamp) => {
    if (!timestamp){
        return
    }


    const dateObject = new Date(timestamp);


    const options = {
        hour: 'numeric',
        minute: 'numeric',
        day: 'numeric',
        month: 'long',
        year: 'numeric',
        timeZone: 'Asia/Almaty', // Adjust the time zone as needed
    };


    const dateFormatter = new Intl.DateTimeFormat('ru-RU', options);


    const formattedDate = dateFormatter.format(dateObject);


    return `${formattedDate}`;
};


export const ManagerPage = () => {


    const navigate = useNavigate();


    useEffect(() => {
        if (!localStorage.getItem("access_token") || localStorage.getItem("role") !== "manager") {
            // navigate("/main");
        }
    }, [navigate])
   
    const [application_data, setApplication_data] = useState();
    const [selectedApplicationData, setSelectedApplicationData] = useState();
    const [loading, setLoading] = useState(true);


    const [selectedAppId, setSelectedAppId] = useState();
    const [viewed_applications, setViewedApplications] = useState([]);

    useEffect(() => {
      setViewedApplications([]); // Assign an empty list on page load
    }, []);

    useEffect(() => {
        // setApplication_data(Applications);
        const fetchData = async () => {
            try {
                const accessToken = localStorage.getItem("access_token");


                const response = await fetch('http://localhost:8000/manager_requests', {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${accessToken}`,
                        'Content-Type': 'application/json',
                    },
                });


                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }


                const data = await response.json();
                setApplication_data(data.data);
                // console.log(data.data);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching data:', error.message);
                setLoading(false);
            }
        };


        fetchData();
    }, []);


    const [openMocal, setOpenModal] = useState(false);


    const onRowClickHandler = (id) => {
        setLoading(true);
        setOpenModal(true);
        console.log(id)
        const fetchData = async () => {
            try {
                const accessToken = localStorage.getItem("access_token");


                setSelectedAppId(id);


                const response = await fetch('http://localhost:8000/manager_requests_detail', {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${accessToken}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        id_app: id,
                    })
                });


                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }


                const data = await response.json();
                console.log(data)
                setSelectedApplicationData(data.data);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching data:', error.message);
                setOpenModal(false);
                setLoading(false);
            }
        };


        fetchData();
        // setSelectedApplicationData(application_data.filter((el)=>el.id===id)[0])
    }


    const sendRequestHandle = (requestStatus) => {
        setViewedApplications(list => [...list, selectedAppId]);
        setOpenModal(false);
        const fetchData = async () => {
            try {
                const accessToken = localStorage.getItem("access_token");




                const response = await fetch('http://localhost:8000/manager_requests_response', {
                    method: 'POST',
                    headers: {
                        'Authorization': `Bearer ${accessToken}`,
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                     id_app: selectedAppId,
                     status: requestStatus
                    })
                });


                if (!response.ok) {
                    throw new Error('Failed to fetch data');
                }


                const data = await response.json();
                console.log(data)
                // setSelectedApplicationData(data);
            } catch (error) {
                console.error('Error fetching data:', error.message);
                setLoading(false);
            }
        };


        fetchData();
       
    }


    return (
        <section className="d-flex">
            {loading ? (
                <div style={{ position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)'}}>
                    <DotLoader color={'#06691a'} loading={loading} size={100}/>
                </div>
            ) : (
                <div>
                {openMocal ? (
                    <div className={classes.modalWrapper}>
                        <button className={classes.closeBtn} onClick={()=>setOpenModal(false)}><img src={require("../../../assets/images/close.png")} alt="close-icon" width={40} height={40}/></button>
                        <div className="d-flex justify-content-around">
                            <div>
                                <table className={classes.appDetailTable}>
                                <thead>
                                    <tr className={classes.tr}>
                                        <th>Поле</th>
                                        <th>Изменения</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {selectedApplicationData && selectedApplicationData.map((item, index) => (
                                        <tr key={`${item.id}-${index}-app_detail-data`} className={classes.tr}>
                                            <td style = {{padding: '0 0 15px 0'}}>{item.column}:</td>
                                            <td>
                                                <div>
                                                    <p className={classes.old_value}>{item.old_value}</p>
                                                    <img src={require("../../../assets/images/arrow-right2.png")} alt="arrow-right" width={32} height={18}/>
                                                    <p className={classes.new_value}>{item.new_value}</p>
                                                </div>
                                            </td>
                                        </tr>
                                    ))}
                                </tbody>
                            </table>
                            </div>
                        </div>
                        <div className="d-flex justify-content-between my-4 mx-5">
                            <button className={classes.btnWrapper} onClick={()=>setOpenModal(false)}>Вернуться к заявкам</button>
                            <div className="d-flex">
                                <button className={classes.btnWrapper} onClick={()=>sendRequestHandle(0)}>Отклонить</button>
                                <button className={classes.btnWrapper} onClick={()=>sendRequestHandle(2)}>Подтвердить</button>
                            </div>
                        </div>
                    </div>
                ) : (
                    <div>
                    <div>
                    {application_data?.length > 0 && application_data?.length > viewed_applications?.length ? (
                        <table className={classes.appDetailTable}>
                            <thead>
                                <tr className={classes.tr}>
                                    <th>Услуги</th>
                                    <th>Номер</th>
                                    <th>Дата отправки заявки</th>
                                </tr>
                            </thead>
                            <tbody>
                                {application_data.map((item, index) => (
                                    !viewed_applications.includes(item.id) && (
                                        <tr key={`${item.id}-${index}-app-data`}  className={'$classes.rowWrapper $classes.tr'} onClick={()=>onRowClickHandler(item.id)}>
                                            <td>Заявка на изменение профиля</td>
                                            <td>{item.id}</td>
                                            <td>{formatTimestamp(item.create_dttm)}</td>
                                        </tr>
                                    )
                                ))}
                            </tbody>
                        </table>
                        ) : (
                            <div className={classes.emptylist}>
                                <img src={require("../../../assets/images/empty-list.png")} alt="empty-list" width={250} height={250}/>
                                <p style={{marginTop: '10px', marginLeft: '50px'}}>Список заявок пуст.</p>
                            </div>
                    )}
                    </div>
                </div>
                )}
                </div>
            )}
        </section>
    )
}