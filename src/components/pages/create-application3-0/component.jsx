import React, { useState } from 'react';
import { Form, Button, Alert } from 'react-bootstrap';
import { DotLoader } from 'react-spinners';

export const CreateApplication3 = () => {
    const [organizationInfo, setOrganizationInfo] = useState({
        iinbin: '',
        name: '',
    });

    const [loading, setLoading] = useState(false);
    const [formSubmitted, setFormSubmitted] = useState(false);
    const [organizationNotFound, setOrganizationNotFound] = useState(false);
    const [startRequestEnabled, setStartRequestEnabled] = useState(false);

    const handleCheckOrganization = async () => {
        try {
            setLoading(true);
            const accessToken = localStorage.getItem("access_token");

            const response = await fetch('http://localhost:8000/check_company', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${accessToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    iinbin: organizationInfo.iinbin,
                    name: organizationInfo.name,
                }),
            });

            if (!response.ok) {
                throw new Error('Failed to check organization');
            }

            const result = await response.json();

            if (result.success) {
                setOrganizationInfo({
                    ...organizationInfo,
                    name: result.company_name,
                });
                setOrganizationNotFound(false);
                setStartRequestEnabled(true);
            } else {
                setOrganizationNotFound(true);
                setStartRequestEnabled(false);
            }
        } catch (error) {
            console.error('Error checking organization:', error.message);
        } finally {
            setLoading(false);
        }
    };

    const handleStartRequest = async () => {
        try {
            setLoading(true);
            const accessToken = localStorage.getItem("access_token");

            const response = await fetch('http://localhost:8000/submit_application', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${accessToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    iinbin: organizationInfo.iinbin,
                    company_name: organizationInfo.name,
                }),
            });

            if (!response.ok) {
                throw new Error('Failed to submit application');
            }

            setFormSubmitted(true);
        } catch (error) {
            console.error('Error submitting application:', error);
        } finally {
            setLoading(false);
        }
    };

    const telegramBotLink = 'https://t.me/egovMagmam_bot';
    return (
        <section className="d-flex">
            <div className="create-application3">
                <Form>
                    <Form.Group controlId="bin">
                        <Form.Label>БИН/ИИН:</Form.Label>
                        <Form.Control
                            type="text"
                            value={organizationInfo.iinbin}
                            onChange={(e) => setOrganizationInfo({ ...organizationInfo, iinbin: e.target.value })}
                        />
                    </Form.Group>

                    <Form.Group controlId="name">
                        <Form.Label>Наименование организации:</Form.Label>
                        <Form.Control
                            type="text"
                            value={organizationInfo.name}
                            onChange={(e) => setOrganizationInfo({ ...organizationInfo, name: e.target.value })}
                        />
                    </Form.Group>

                    <Button
                        variant="primary"
                        onClick={handleCheckOrganization}
                        disabled={loading || formSubmitted}
                    >
                        Проверить Организацию
                    </Button>

                    {organizationNotFound && (
                        <Alert variant="danger" className="alert">
                            Организация не найдена. Пожалуйста, проверьте информацию.
                        </Alert>
                    )}

                    {startRequestEnabled && !formSubmitted && (
                        <Button
                            variant="success"
                            onClick={handleStartRequest}
                            disabled={loading || organizationNotFound || formSubmitted}
                        >
                            Запустить Заявку
                        </Button>
                    )}

                    {startRequestEnabled && !formSubmitted && (
                        <Alert variant="success" className="alert">
                            Организация найдена. Перед тем как отправить заявку свяжитесь с ботом, чтобы получить PDF файл.<a href={telegramBotLink} target="_blank" rel="noopener noreferrer"><b>Связаться с ботом</b></a>
                        </Alert>
                    )}

                    {loading && (
                        <div>
                            <Alert variant="success" className="alert">
                                Ваша заявка успешно отправлено.Ожидайте информацию об организации в телеграм боте и на почте.
                            </Alert>
                        </div>
                    )}
                    {formSubmitted && (
                        <div>
                            <Alert variant="success" className="alert">
                                Ваша заявка успешно отправлено.Ожидайте информацию об организации в телеграм боте и на почте.
                            </Alert>
                        </div>
                    )}

                </Form>
            </div>
        </section>
    );
}
